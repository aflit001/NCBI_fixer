#!/usr/bin/python
import os, sys
import pprint
import argparse
import re
MIN_SEQ_LEN = 190
LINELEN     =  80
writefasta  = False

#python NCBI_fixer.py -i habrochaites/final.assembly.fasta -r 'scaffold_(\d+)/Sh_\1 [organism=Solanum habrochaites][cultivar=LYC4]' -x scaffold_5444 -t scaffold_5444:50..70


#reset; python NCBI_fixer.py -s habrochaites/final.assembly.fasta  -c habrochaites/final.contigs.fasta -a habrochaites/final.summary -b habrochaites/agp_conf_allpathslg.conf -r 'scaffold_(\d+)/Sh_scaffold_\1 [organism=Solanum habrochaites][cultivar=LYC4]' -q 'contig_(\d+)/Sh_contig_\1 [organism=Solanum habrochaites][cultivar=LYC4]' -t scaffold_2850:498..531
#reset; python NCBI_fixer.py -s pennellii/final.assembly.fasta     -c pennellii/final.contigs.fasta    -a pennellii/final.summary    -b pennellii/agp_conf_allpathslg.conf    -r 'scaffold_(\d+)/Sp_scaffold_\1 [organism=Solanum pennellii][cultivar=LA0716]'  -q 'contig_(\d+)/Sp_contig_\1 [organism=Solanum pennellii][cultivar=LA0716]'         -x scaffold_21146 -x scaffold_22682 -x scaffold_24160 -t scaffold_2137:76803..76835 -t scaffold_326:162553..162586 -t scaffold_6481:27699..27732 -t scaffold_718:6166..6199 -t scaffold_7926:13484..13517
#reset; python NCBI_fixer.py -s arcanum_2/final.assembly.fasta     -c arcanum_2/final.contigs.fasta    -a arcanum_2/final.summary    -b arcanum_2/agp_conf_allpathslg.conf    -r 'scaffold_(\d+)/Sa_scaffold_\1 [organism=Solanum arcanum][cultivar=LA2157]'    -q 'contig_(\d+)/Sa_contig_\1 [organism=Solanum arcanum][cultivar=LA2157]'
#reset; python NCBI_fixer.py                                       -c arcanum/CLC-830MB-tryout2.fa                                                                                                                                                              -q '120524_I238_FCD1174ACXX_L2_SZAXPI009264-133_1.fq.gz.clean.dup_\(paired\)_contig_(\d+)/Sa_contig_\1 [organism=Solanum arcanum][cultivar=LA2157]'
#
#./agp_validate habrochaites/final.assembly.fasta.good.fasta habrochaites/final.contigs.fasta.good.fasta habrochaites/final.assembly.fasta.agp
#./agp_validate pennellii/final.assembly.fasta.good.fasta    pennellii/final.contigs.fasta.good.fasta    pennellii/final.assembly.fasta.agp
#./agp_validate arcanum_2/final.assembly.fasta.good.fasta    arcanum_2/final.contigs.fasta.good.fasta    arcanum_2/final.assembly.fasta.agp

#reset; rm habrochaites/final.assembly.fasta.agp; python NCBI_fixer.py -n -a habrochaites/final.summary -b habrochaites/agp_conf_allpathslg.conf ; if [[ $? == 0 ]]; then ./agp_validate habrochaites/final.assembly.fasta habrochaites/final.contigs.fasta habrochaites/final.summary.agp; fi
#reset; rm pennellii/final.assembly.fasta.agp;    python NCBI_fixer.py -n -a pennellii/final.summary    -b pennellii/agp_conf_allpathslg.conf ;    if [[ $? == 0 ]]; then ./agp_validate pennellii/final.assembly.fasta    pennellii/final.contigs.fasta    pennellii/final.summary.agp;    fi


#ln -s final.assembly.fasta.good.fasta Shabrochaites_scaffold.fsa
#ln -s final.contigs.fasta.good.fasta  Shabrochaites_contig.fsa
#mkdir scaffold
#mkdir contig
#cd scaffold
#ln -s ../Shabrochaites_scaffold.fsa .
#cd ..
#cd contig
#ln -s ../Shabrochaites_contig.fsa
#cd ../..
#./tbl2asn -t habrochaites/template.sbt -p habrochaites/scaffold -z habrochaites/scaffold/discrep -n 'Solanum habrochaites' -N 0.1 -V bt -T -M b -W &
#./tbl2asn -t habrochaites/template.sbt -p habrochaites/contig   -z habrochaites/contig/discrep   -n 'Solanum habrochaites' -N 0.1 -V bt -T -M b -W &



#ln -s final.assembly.fasta.good.fasta Spennellii_scaffold.fsa
#ln -s final.contigs.fasta.good.fasta  Spennellii_contig.fsa
#mkdir scaffold
#mkdir contig
#cd scaffold
#ln -s ../Spennellii_scaffold.fsa .
#cd ..
#cd contig
#ln -s ../Spennellii_contig.fsa
#cd ../..
#./tbl2asn -t pennellii/template.sbt    -p pennellii/scaffold    -z pennellii/scaffold/discrep    -n 'Solanum pennellii'    -N 0.1 -V bt -T -M b -W &
#./tbl2asn -t pennellii/template.sbt    -p pennellii/contig      -z pennellii/contig/discrep      -n 'Solanum pennellii'    -N 0.1 -V bt -T -M b -W &



#./tbl2asn -t habrochaites/template.sbt -p habrochaites/scaffold -z habrochaites/scaffold/discrep -n 'Solanum habrochaites' -N 0.1 -V bt -T -M b -W &
#./tbl2asn -t habrochaites/template.sbt -p habrochaites/contig   -z habrochaites/contig/discrep   -n 'Solanum habrochaites' -N 0.1 -V bt -T -M b -W &
#./tbl2asn -t pennellii/template.sbt    -p pennellii/scaffold    -z pennellii/scaffold/discrep    -n 'Solanum pennellii'    -N 0.1 -V bt -T -M b -W &
#./tbl2asn -t pennellii/template.sbt    -p pennellii/contig      -z pennellii/contig/discrep      -n 'Solanum pennellii'    -N 0.1 -V bt -T -M b -W &
#./tbl2asn -t arcanum_2/template.sbt    -p arcanum_2/scaffold    -z arcanum_2/scaffold/discrep    -n 'Solanum arcanum'      -N 0.2 -V bt -T -M b -W &
#./tbl2asn -t arcanum_2/template.sbt    -p arcanum_2/contig      -z arcanum_2/contig/discrep      -n 'Solanum arcanum'      -N 0.2 -V bt -T -M b -W &



#./tbl2asn -t arcanum/template.sbt      -p arcanum/      -z arcanum/discrep      -n 'Solanum arcanum'      -N 0.1 -V bt -T -M b

class trimmer(object):
    def __init__(self):
        self.rules = []

    def add(self, start, end):
        self.rules.append( [start, end] )

    def trim(self, seq):
        lenseq = len(seq)

        for rule in self.rules:
            start, end = rule

            print "trimming. start",start,"end",end,"length",len(seq)
            effStart = start - 1
            if effStart < 0:
                effStart = 0

            #p0  = seq[0      :end  +10 ]
            p1  = seq[0      :effStart ]
            #p2  = seq[effStart:end     ]
            p3  = ''.join( ['N']*(end-start+1) )
            p4  = seq[end    :         ]

            seq = p1 + p3 + p4
            #print p0
            #print p1
            #print ' '*(effStart) + p2
            #print ' '*(effStart) + p3
            #print ' '*(end     )+ p4[:10]
            #print p0
            #print seq[:end+10]

            if len(seq) != lenseq:
                print "new length not equal to old length", len(seq), lenseq
                sys.exit(1)

        return seq

def trimends(seq):
    start = 0
    end   = -1

    #print "trimming ends. length",len(seq)

    for p in xrange(len(seq)):
        n = seq[ p ]
        if start == 0 and n != 'N':
            #print "BEGIN N",n,"P",p
            start = p
            break

    for p in xrange(len(seq)-1, 0, -1):
        #print "trim. P",p
        n = seq[p]
        if end == -1 and n != 'N':
            #print "END   N",n,"P",p
            end = p
            break


    if start != 0 and end != len(seq):
        #print "  trimming ends. start",start,'end',end,'length',len(seq),'length after',len(seq),'equal',true
        return [seq, 0, 0]
    else:
        seqnew = seq[start:end+1]
        #print "  trimming ends. start",start,'end',end,'length',len(seq),'length after',len(seqnew),'equal',(seq == seqnew)
        return [seqnew, start, len(seq)-end]

class printseq(object):
    statsKey = [ 'SEQ', 'EXC', 'TRIMEND', 'TRIMPOS', 'SEQLEN', 'NPERC', 'NOT_VALID', 'VALID', 'COMPOSITE']
    def __init__(self, infile, exclude=[], min_seq_len=MIN_SEQ_LEN, rename=[], trim=[], linelen=LINELEN, agp=None, contig=False):
        self.infile      = infile
        self.exclude     = exclude
        self.min_seq_len = min_seq_len
        self.rename      = rename
        self.trim        = trim
        self.linelen     = linelen
        self.agp         = agp
        self.contig      = contig


        self.stats       = {}
        for key in self.statsKey:
            self.stats[key] = [ 0, 0 ]
        self.stats['COMPOSITE'] = {}

        self.rencom      = []
        for ren in self.rename:
            ren1, ren2 = ren.split("/")
            ren1 = r'{0}'.format(ren1)
            ren2 = r'{0}'.format(ren2)
            print "COMPILING FROM %s TO %s" % (ren1, ren2)
            self.rencom.append( [re.compile(ren1), ren2] )

        self.trimmer = {}
        for trimnfo in self.trim:
            tname,tpos   = trimnfo.split(":")
            tstart, tend = [int(x) for x in tpos.split("..")]
            if tname not in self.trimmer:
                self.trimmer[tname] = trimmer()
            self.trimmer[tname].add( tstart, tend )

        if writefasta:
            self.outgood     = infile + '.good.fasta'
            self.outbad      = infile + '.bad.fasta'
            print "OPENING GOOD", self.outgood
            print "OPENING BAD ", self.outbad
            self.goodfhd     = open(self.outgood, 'w')
            self.badfhd      = open(self.outbad , 'w')

        self.outlog      = infile + '.log'
        print "OPENING LOG ", self.outlog
        self.logfhd      = open(self.outlog , 'w')

        self.ofhd        = None

    def close(self):
        self.logfhd.write( "STATS\n" )
        print "STATS"

        for key in sorted( self.stats ):
            if key == 'COMPOSITE':
                pass
            else:
                self.logfhd.write("SINLGE\t%10s\t%d\t%d\n" % (key, self.stats[key][0], self.stats[key][1]))
                print "SINGLE    %15s %7d %10d" % (key, self.stats[key][0], self.stats[key][1])

        for key in sorted( self.stats['COMPOSITE'] ):
            self.logfhd.write("COMPOSITE\t%10s\t%d\t%d\n" % (key, self.stats['COMPOSITE'][key][0], self.stats['COMPOSITE'][key][1]))
            print "COMPOSITE %15s %7d %10d" % (key, self.stats['COMPOSITE'][key][0], self.stats['COMPOSITE'][key][1])

        self.logfhd.close()
        if writefasta:
            self.goodfhd.close()
            self.badfhd.close()

    def add(self, seqname, seqseq):
        seqlen1 = len(seqseq)
        #print "SEQ",seqname,"LENGTH",seqlen1
        nlen    = 0
        valid   = True
        verb    = ''
        reasons = []

        self.stats['SEQ'][0] += 1
        self.stats['SEQ'][1] += seqlen1

        if seqname in self.exclude:
            verb  += "  excluding\n"
            valid  = False
            reasons.append( 'EXC' )
            self.stats['EXC'][0] += 1
            self.stats['EXC'][1] += seqlen1

            if self.agp is not None:
                if ( not self.contig ):
                    if seqname not in self.agp['forbidden scaffolds']:
                        self.agp['forbidden scaffolds'].append( seqname )
                else:
                    if seqname not in self.agp['forbidden contigs']:
                        self.agp['forbidden contigs'].append( seqname )




        seqb                         = seqseq
        seqseq, trim_start, trim_end = trimends(seqseq)
        if seqb != seqseq:
            reasons.append( 'TRIMEND' )
            self.stats['TRIMEND'][0] += 1
            self.stats['TRIMEND'][1] += seqlen1

            if self.agp is not None:
                if ( not self.contig ):
                    if seqname not in self.agp['kv']:
                        print "SEQUENCE %s NOT IN AGP" % seqname
                        sys.exit( 1 )

                    else:
                        #kv  = self.agp['kv' ].pop( seqname )
                        kv  = self.agp['kv' ][ seqname ]
                        con = self.agp['key'][ kv      ]
                        #[ contig_id, gap, std_dev, contig_len, contig_start, contig_end ]
                        #  0          1    2        3           4             5

                        if trim_start != 0:
                            con[0][4]  += trim_start

                        if trim_end != 0:
                            con[-1][5] -= trim_end
                else:
                    self.agp['trimmed contigs'].append( [seqname, trim_start, trim_end] )


        seqlen2 = len(seqseq)


        seqb    = seqseq
        if seqname in self.trimmer:
            seqseq = self.trimmer[seqname].trim( seqseq )

        if seqb != seqseq:
            reasons.append( 'TRIMPOS' )
            self.stats['TRIMPOS'][0] += 1
            self.stats['TRIMPOS'][1] += seqlen2
            #TODO
            #if self.agp is not None:
            #    if ( not self.contig ):
            #        if seqname not in self.agp['kv']:
            #            print "SEQUENCE %s NOT IN AGP" % seqname
            #            sys.exit( 1 )
            #
            #        else:
            #
            #    else:


        seqlen3 = len(seqseq)


        if seqlen3 <= self.min_seq_len:
            verb += "  too short\n"
            valid  = False
            reasons.append( 'SEQLEN' )
            self.stats['SEQLEN'][0] += 1
            self.stats['SEQLEN'][1] += seqlen3


        nperc = 0
        for x in seqseq:
            if x == 'N':
                nlen += 1

        if nlen > 1:
            nperc = nlen / float(seqlen3)
            if nperc >= .5:
                verb += "  too many Ns\n"
                valid = False
                reasons.append( 'NPERC' )
                self.stats['NPERC'][0] += 1
                self.stats['NPERC'][1] += seqlen3


        if writefasta:
            self.ofhd     = self.goodfhd

        seqnameb = seqname

        if not valid:
            if writefasta:
                self.ofhd = self.badfhd

            print "skipping  ",
            self.stats['NOT_VALID'][0] += 1
            self.stats['NOT_VALID'][1] += seqlen3
            if self.agp is not None:
                if ( not self.contig ):
                    if seqname not in self.agp['kv']:
                        print "SEQUENCE %s NOT IN AGP" % seqname
                        sys.exit( 1 )

                    else:
                        kv  = self.agp['kv' ].pop( seqname )
                        con = self.agp['key'].pop( kv      )
                        print "DELETING", kv, 'FROM AGP', con
                else:
                    if seqname not in self.agp['forbidden contigs']:
                        self.agp['forbidden contigs'].append( seqname )




        else:
            reasons.append( 'OK' )
            for rule in self.rencom:
                #print " applying rule", rule[1]
                #r = rule[0].search( seqname )
                #print "  R", r, rule[0]
                seqname = rule[0].sub( rule[1], seqname )

            print "exporting ",
            self.stats['VALID'][0] += 1
            self.stats['VALID'][1] += seqlen3

        reasonsStr = "|".join( reasons )
        print "'%s' => '%s'. VALID %s SEQ LEN 1 %5d SEQ LEN 2 %5d SEQ LEN 3 %5d PERC N %6.3f REASONS %s" % ( seqnameb, seqname, str(valid), seqlen1, seqlen2, seqlen3, nperc, reasonsStr )
        self.logfhd.write( "\t".join( [str(x) for x in [seqnameb, seqname, str(valid), seqlen1, seqlen2, seqlen3, nperc, reasonsStr]]) + "\n" )
        sys.stdout.write( verb )

        if reasonsStr not in self.stats['COMPOSITE']:
            self.stats['COMPOSITE'][reasonsStr] = [0, 0]
        self.stats['COMPOSITE'][reasonsStr][0] += 1
        self.stats['COMPOSITE'][reasonsStr][1] += seqlen3


        if writefasta:
            self.ofhd.write( ">%s\n" % seqname )
            for start in xrange(0, seqlen3, self.linelen):
                end = start + self.linelen
                if end > seqlen3:
                    end = seqlen3
                frag = seqseq[start:end]

                #print seqname, seqlen, start, end, frag
                self.ofhd.write( frag + "\n" )
            self.ofhd.write( "\n" )



def parseFile( args, infile, agp , contig=False):
    rename  = args.srename
    if contig:
        rename = args.crename
    printer = printseq(infile, exclude=args.exclude, min_seq_len=args.min_seq_len, rename=rename, trim=args.trim, agp=agp, contig=contig)

    with open(infile, 'r') as fhd:
        seqname = None
        seqseq  = ""

        for line in fhd:
            line = line.strip()

            if len(line) == 0:
                continue

            if line[0] == ">":
                if seqseq != "" and seqname is not None:
                    printer.add(seqname, seqseq)

                seqname = line[1:]
                seqseq  = ""
                continue

            seqseq += line

        if seqseq != "" and seqname is not None:
            printer.add(seqname, seqseq)

    printer.close()



def parseAgp( agp, agpconf, args ):
    if not os.path.exists( agp ):
        print "agp file %s does not exists" % agp
        sys.exit( 1 )

    data = {
        'kv'     : {},
        'nums'   : {},
        'key'    : {},
        'conf'   : {},
        'forbidden contigs'  : [],
        'forbidden scaffolds': [],
        'trimmed contigs'    : [],
        'headers': {
            'fields' : {
                'scaffold prefix' :'scaffold_',
                'contig prefix'   :'contig_',
                'component type'  :'P',
                'gap type'        :'scaffold',
                'linkage'         :'yes',
                'orientation'     :'?',
                'linkage evidence':'paired-ends'
            },
            'header': {
                'SOURCE FILE': agp
            }
        },
    }


    srename  = args.srename
    crename  = args.crename

    srencom      = []
    for ren in srename:
        ren1, ren2 = ren.split("/")
        ren1 = r'{0}'.format(ren1)
        ren2 = r'{0}'.format(ren2)
        print "COMPILING FROM %s TO %s" % (ren1, ren2)
        srencom.append( [re.compile(ren1), ren2] )



    crencom      = []
    for ren in crename:
        ren1, ren2 = ren.split("/")
        ren1 = r'{0}'.format(ren1)
        ren2 = r'{0}'.format(ren2)
        print "COMPILING FROM %s TO %s" % (ren1, ren2)
        crencom.append( [re.compile(ren1), ren2] )





    if agpconf is not None:
        if not os.path.exists( agpconf ):
            print "agp file %s does not exists" % agp
            sys.exit( 1 )

        with open(agpconf, 'r') as fhd:
            for line in fhd:
                line = line.strip()

                if len(line) == 0:
                    continue

                if line[0] == "#":
                    continue

                pos = line.find('=')

                if pos == -1:
                    continue

                name = line[     :pos].strip()
                val  = line[pos+1:   ].strip()

                kvpos = name.find(':')
                if kvpos != -1:
                    k = name[       :kvpos]
                    v = name[kvpos+1:     ]

                    if k not in data['headers']:
                        data['headers'][k] = {}

                    data['headers'][k][v] = val
                    #print "adding header K %s V %s Val %s" % ( str(k), str(v), str(val) )

                else:
                    data['conf'][name] = val
                    #print "adding cong Name %s  Val %s" % ( str(name), str(val) )

    fields           = data['headers']['fields']
    #print "FIELDS", fields
    scaffold_prefix  = fields['scaffold prefix' ]
    contig_prefix    = fields['contig prefix'   ]


    #pprint.pprint( data )
    #sys.exit(0)

    with open(agp, 'r') as fhd:
        last_scaffold = None
        for line in fhd:
            line = line.strip()

            #print line

            if '(l =' not in line:
                continue

            if   line.startswith('scaffold'): #scaffold id
                cols                       = line.split(" ")
                #print cols
                scaffold_id                = scaffold_prefix + cols[1]

                scaffold_id_ren            = scaffold_id
                for rule in srencom:
                    #print " applying rule", rule[1]
                    #r = rule[0].search( seqname )
                    #print "  R", r, rule[0]
                    scaffold_id_ren = rule[0].sub( rule[1], scaffold_id_ren )

                scaffold_id_ren = scaffold_id_ren.split(" ")[0]

                scaffold_len               = int( cols[4][:-2] )
                scaffold_num               = int( cols[1]      )
                last_scaffold              = (scaffold_id, scaffold_id_ren, int(cols[1]), scaffold_len)
                data['kv'  ][scaffold_id  ] = last_scaffold
                data['nums'][int(cols[1]) ] = scaffold_id
                data['key' ][last_scaffold] = []

            elif line.startswith('-- '): #middle/end contig with split information
                cols          = line.split(" ")
                #print cols
                contig_id     = contig_prefix + cols[5]
                contig_id_ren = contig_id
                for rule in crencom:
                    #print " applying rule", rule[1]
                    #r = rule[0].search( seqname )
                    #print "  R", r, rule[0]
                    contig_id_ren = rule[0].sub( rule[1], contig_id_ren )
                contig_id_ren = contig_id_ren.split(" ")[0]

                gap          = int( cols[1][1:  ] )
                std_dev      = int( cols[3][ :-1] )
                contig_len   = int( cols[8][ :-1] )
                contig_start = 1
                contig_end   = contig_len + 1
                data['key'][last_scaffold].append( [ contig_id, contig_id_ren, gap, std_dev, contig_len, contig_start, contig_end ] )

            else: #first contig
                cols          = line.split(" ")
                #print cols
                contig_id     = contig_prefix + cols[0]
                contig_id_ren = contig_id
                for rule in crencom:
                    #print " applying rule", rule[1]
                    #r = rule[0].search( seqname )
                    #print "  R", r, rule[0]
                    contig_id_ren = rule[0].sub( rule[1], contig_id_ren )
                contig_id_ren = contig_id_ren.split(" ")[0]

                contig_len   = int( cols[3][ :-1]  )
                contig_start = 1
                contig_end   = contig_len + 1
                data['key'][last_scaffold].append( [ contig_id, contig_id_ren, None, None, contig_len, contig_start, contig_end ] )

    return data



def printAgp(data, ofile):
    #pprint.pprint( data )

    #http://www.ncbi.nlm.nih.gov/projects/genome/assembly/agp/AGP_Specification.shtml
    """
    ##agp-version	2.0
    # ORGANISM: Homo sapiens
    # TAX_ID: 9606
    # ASSEMBLY NAME: EG1
    # ASSEMBLY DATE: 09-November-2011
    # GENOME CENTER: NCBI
    # DESCRIPTION: Example AGP specifying the assembly of scaffolds from WGS contigs
    EG1_scaffold1	1       3043	1	W	AADB02037551.1	1	        3043	+

    EG1_scaffold2	1       40448	1	W	AADB02037552.1	1	        40448	+
    EG1_scaffold2	40449	40661	2	N	213	            scaffold	yes	    paired-ends
    EG1_scaffold2	40662	117642	3	W	AADB02037553.1	1	        76981	+
    EG1_scaffold2	117643	117718	4	N	76	            scaffold	yes	    paired-ends
    EG1_scaffold2	117719	145387	5	W	AADB02037554.1	1	        27669	+

    EG1_scaffold6	1	    4590	1	W	AADB02037584.1	1	        4590	+
    EG1_scaffold6	4591	4781	2	N	191	            scaffold	yes	    paired-ends
    EG1_scaffold6	4782	77946	3	W	AADB02037585.1	1	        73165	+
    """


    print "OPENING",ofile
    ofhd     = open(ofile, 'w')
    ofhd.write("##agp-version	2.0\n")
    headers  = data['headers']['header']
    fields   = data['headers']['fields']

    for header in sorted(headers):
        ofhd.write( "#%s:%s\n" % ( header, headers[header] ) )
    ofhd.write( "#%s:%s\n" % ( sys.argv[0].upper(), " ".join(sys.argv) ) )

    component_type   = fields['component type'  ]
    gap_type         = fields['gap type'        ]
    linkage          = fields['linkage'         ]
    orientation      = fields['orientation'     ]
    linkage_evidence = fields['linkage evidence']
    print "component type", component_type
    print "gap type      ", gap_type
    print "linkage       ", linkage
    print "orientation   ", orientation
    print "linkage ev    ", linkage_evidence


    data_key = data['key' ]

    labels = [
        'contig id'            , 'contig id ren'          , 'gap'                        , 'std dev',
        'contig len'           , 'contig start'           , 'contig end'                 ,
        #0                       1                          2                              3
        "gap>0"                , 'gap overlap start begin', 'gap overlap end begin'      , 'gap start begin',
        'gap end begin'        , 'gap begin diff'         , 'gap <= begindiff'           , 'effstart',
        "gap overlap start end", 'gap overlap ens end'    , 'gap size end'               , 'gap start end',
        'gap end end'
        #0                       1                          2                              3
        'con start begin'      , 'con contig len begin'   , 'con overlap start begin'    , 'con overlap end begin',
        'con overlapstart > 0' , 'con begin diff'         , 'con contig_len >= begindiff', 'con contig_len end',
        'con contig start end' , 'con overlap start end'  , 'con overlap end end'        , 'con start end',
        'con end end'          , 'con print'              , 'con final begin'            , 'con final end',
        'con start last'       , 'con count'
    ]


    #self.agp['forbidden contigs']
    #self.agp['trimmed contigs'].append( [seqname, trim_start, trim_end] )
    #self.agp['forbidden scaffolds'].append( seqname )

    print "FORBIDDEN SCAFFOLDS", data['forbidden scaffolds' ]
    print "FORBIDDEN CONTIGS  ", data['forbidden contigs'   ]
    print "TRIMMED CONTIGS    ", data['trimmed contigs'     ]


    for scaffold_key in sorted(data_key, key=lambda data_key: data_key[2]):
        scaffold_id, scaffold_id_ren, scaffold_num, scaffold_len = scaffold_key
        contig_data = data_key[scaffold_key]

        if scaffold_id in data['forbidden scaffolds' ]:
            print "FOBIDDEN SCAFFOLD", scaffold_id

        start        = 1
        end          = 1
        final        = 1
        count        = 1
        lines        = []
        overlapstart = 0
        overlapend   = 0
        log          = []
        log.append('scaffold ' + str(scaffold_key))

        for contig_pos in range(len(contig_data)):
            contig_curr = contig_data[contig_pos]
            contig_id, contig_id_ren, gap, std_dev, contig_len, contig_start, contig_end = contig_curr
            log.append(" contig id     %s contig len %8d start %8d" % (contig_id, contig_len, start))

            if contig_id in data['forbidden contigs'   ]:
                log.append("  FORBIDDEN CONTIG")
                log.append("  conntig len  %8d" % contig_len)

                if gap is not None:
                    log.append("  gap BEFORE   %8d" % gap       )
                    gap += contig_len
                    log.append("  gap AFTER    %8d" % gap       )

                else:
                    log.append("  gap BEFORE   %8d" % gap       )
                    gap = contig_len
                    log.append("  gap AFTER    %8d" % gap       )



            #          0         2         4         6         8         10        12        14        16        18
            gapvals = [None,None,None,None,None,None,None,None,None,None,None,None,None]
            convals = [None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None]

            enddiff     = 0
            if gap is not None:
                log.append("  gap          %8d" % gap)
                gapvals[0] = gap > 0
                gapvals[1] = overlapstart
                gapvals[2] = overlapend
                gapvals[3] = start
                gapvals[4] = end

                if gap > 0:
                    log.append("   gap > 0")
                    log.append("    start        %8d" % start)

                    printgap     = True
                    effstart     = start
                    if overlapstart != 0:
                        log.append("    overlapstart %d != 0" % overlapstart)
                        begindiff          = start - overlapstart
                        log.append("     begindiff    %8d " % begindiff)
                        gapvals[5] = begindiff
                        gapvals[6] = gap <= begindiff

                        if gap <= begindiff:
                            log.append("     skipping gap")
                            gapoverlap     = abs(gap)
                            log.append("      overlapstart   %8d " % overlapstart)
                            overlapstart   += gapoverlap
                            log.append("      overlapstart   %8d " % overlapstart)
                            log.append("      overlapend     %8d " % overlapend)
                            overlapend     += gapoverlap
                            log.append("      overlapend     %8d " % overlapend)
                            printgap       = False

                        else:
                            log.append("     real gap")
                            log.append("      begindiff    %8d " % begindiff)
                            gap            = gap - begindiff
                            log.append("      gap          %8d " % gap)
                            log.append("      RESETING OVERLAP")
                            overlapstart   = 0
                            overlapend     = 0

                    else:
                        log.append("    overlapend %d == 0" % overlapend)
                        log.append("    normal gap")

                    if printgap:
                        end    = start + gap - 1

                        log.append("    gap          %8d" % gap)
                        log.append("    end          %8d" % end)

                        assert start        >            0, "\n".join( log )
                        assert end          >            0, "\n".join( log )
                        assert end          >=       start, "\n".join( log )
                        assert gap          >            0, "\n".join( log )

                        lines.append( [ scaffold_id_ren, start, end, count, 'N', gap, gap_type, linkage, linkage_evidence, True ] )
                        start  = end   + 1
                        final += gap
                        count += 1


                elif gap < 0:
                    log.append("   gap < 0")
                    gapoverlap       = abs(gap)
                    log.append("    gap          %8d " % gap)
                    log.append("    gapoverlap   %8d " % gapoverlap)

                    effstart     = start
                    if overlapend != 0:
                        effstart = overlapend
                    gapvals[7 ] = effstart

                    log.append("    start        %8d" % start)
                    log.append("    overlapstart %8d" % overlapstart)
                    log.append("    overlapend   %8d" % overlapend)
                    log.append("    eff start    %8d (start        %8d | overlapend   %8d)" % (effstart    , start         ,overlapend ))


                    overlapstart   = effstart     - gapoverlap
                    overlapend     = overlapstart + gapoverlap
                    log.append("    overlapstart %8d (effstart     %8d - gapoverlap   %8d) %d" % (overlapstart, effstart    , gapoverlap, effstart-overlapstart))
                    log.append("    overlapend   %8d (overlapstart %8d + gapoverlap   %8d) %d" % (overlapend  , overlapstart, gapoverlap, overlapend-overlapstart))

                gapvals[8 ] = overlapstart
                gapvals[9 ] = overlapend
                gapvals[10] = gap
                gapvals[11] = start
                gapvals[12] = end

            else:
                log.append("  no gap")


            if contig_id not in data['forbidden contigs'   ]:
                convals[ 0] = start
                convals[ 1] = contig_len
                convals[ 2] = overlapstart
                convals[ 3] = overlapend

                log.append("  contig")

                log.append("   start        %8d" % start       )
                log.append("   contig_len   %8d" % contig_len  )
                log.append("   contig_start %8d" % contig_start)
                log.append("   overlapstart %8d" % overlapstart)
                log.append("   overlapend   %8d" % overlapend  )

                convals[ 4] = overlapstart > 0

                printcontig = True
                if overlapstart > 0:
                    begindiff       = start - overlapstart
                    log.append("   overslap start > 0")
                    log.append("    begindiff    %8d (start        %8d - overlapstart %8d)" % (begindiff   , start         , overlapstart))

                    convals[5] = begindiff
                    convals[6] = contig_len >= begindiff

                    if contig_len >= begindiff:
                        log.append("    valid contig")
                        log.append("     contig len %d >= begindiff %d (%d)" % (contig_len, begindiff, (contig_len-begindiff)))
                        contig_len_o    = contig_len
                        contig_start_o  = contig_start
                        contig_len     -= begindiff
                        contig_start   += begindiff
                        log.append("     contig_len   %8d (contig_len   %8d - begindiff    %8d)" % (contig_len  , contig_len_o  , begindiff   ))
                        log.append("     contig_start %8d (contig_start %8d + begindiff    %8d)" % (contig_start, contig_start_o, begindiff   ))
                        log.append("     RESETING OVERLAP")
                        overlapend      = 0
                        overlapstart    = 0

                    else:
                        log.append("    swallowed contig")
                        log.append("     contig len %d < begindiff %d (%d)" % (contig_len, begindiff, (begindiff-contig_len)))
                        overlapstart_o = overlapstart
                        overlapend_o   = overlapend
                        overlapstart  += contig_len
                        overlapend    += contig_len
                        log.append("      overlapstart %8d (overlapstart %8d + contig_len   %8d)" % (overlapstart, overlapstart_o, contig_len))
                        log.append("      overlapend   %8d (overlapend   %8d + contig_len   %8d)" % (overlapend  , overlapend_o  , contig_len))
                        begindiff      = start - overlapstart
                        log.append("      begindiff    %8d (start        %8d - overlapstart %8d)" % (begindiff   , start         , overlapstart))
                        printcontig    = False

                convals[ 7] = contig_len
                convals[ 8] = contig_start
                convals[ 9] = overlapstart
                convals[10] = overlapend



                end    = start + contig_len - 1
                convals[11] = start
                convals[12] = end
                convals[13] = printcontig
                convals[14] = final
                log.append("   start        %8d" % start      )
                log.append("   end          %8d" % end        )
                log.append("   length       %8d" % (end-start))
                log.append("   contig len   %8d" % contig_len )
                log.append("   final        %8d" % final      )

                lline  = "\t".join([str(x) for x in [ scaffold_id_ren, start, end, count, component_type, contig_id_ren, contig_start, contig_end -1, orientation ]]) + "\n"

                if printcontig:
                    log.append("  +"+lline)
                    if start > end:
                        print 'ERROR - START > END :: %d > %d len %d' % (start, end, contig_len)
                        print "\n".join(log)
                        sys.exit(1)

                    final      += contig_len
                    convals[15] = final

                    assert start        >            0, "\n".join( log )
                    assert end          >            0, "\n".join( log )
                    assert end          >=       start, "\n".join( log )
                    assert contig_start >            0, "\n".join( log )
                    assert contig_end   >            0, "\n".join( log )
                    assert contig_end   > contig_start, "\n".join( log )

                    lines.append( [ scaffold_id_ren, start, end, count, component_type, contig_id_ren, contig_start, contig_end -1, orientation, False ] )

                    start  = end + 1
                    convals[16] = start
                    count += 1
                    convals[17] = count
                else:
                    log.append("  -"+lline)

                #contig_curr.extend(gapvals)
                #contig_curr.extend(convals)




        linestr = ""
        for line in lines:
            linestr += "\t".join([str(x) for x in line[:-1]]) + "\n"
        #print "PRINTING", linestr

        #fixing all paths lg bug where, if coordinates of previous overlap ends
        #before the end of a contig, the contig is trimmed.
        # |--------------| contig 1
        #       |-----|    contig 2 with overlap
        # |-----------|    final assembly
        log.append( "FINAL %d" % final            )
        if overlapstart != 0:
            print "HAS LEFTOVER OVERLAP"
            print "OVERLAP END  ", overlapend
            print "OVERLAP START", overlapstart
            print "PRINTING", linestr

            log.append( "HAS LEFTOVER OVERLAP"            )
            log.append( "OVERLAP START %d" % overlapstart )
            log.append( "OVERLAP END   %d" % overlapend   )
            log.append( "PRINTING\n" + linestr            )
            log.append( "LAST LINE " + "\t".join([str(x) for x in lines[-1]]))


            start  = lines[-1][1]
            end    = lines[-1][2]
            cstart = lines[-1][6]
            cend   = lines[-1][7]

            log.append( "START   b     %d" % start  )
            log.append( "END     b     %d" % end    )
            log.append( "cSTART  b     %d" % cstart )
            log.append( "cEND    b     %d" % cend   )

            assert final == end + 1, "\n".join( log )

            if overlapstart > 0:
                lines[-1][2]  = overlapstart               # end         = overlapstart
                overlapdiff   = overlapstart - start       # overlapdiff = overlapstart - start
                log.append( "OVERLAP DIFF  %d ( overlapstart %d - start %d)" % (overlapdiff, overlapstart, start) )
                lines[-1][7]  = lines[-1][6] + overlapdiff # contig end  = contig start + overlapdiff

                log.append( "LAST LINE " + "\t".join([str(x) for x in lines[-1]]))

                start  = lines[-1][1]
                end    = lines[-1][2]
                cstart = lines[-1][6]
                cend   = lines[-1][7]
                final  = end + 1

                log.append( "START   a     %d" % start  )
                log.append( "END     a     %d" % end    )
                log.append( "cSTART  a     %d" % cstart )
                log.append( "cEND    a     %d" % cend   )

            else:
                newend        = end    + overlapstart + 1
                newlength     = newend - start
                newcend       = newlength + 1

                log.append( "NEW END       %d (  end %d + overlapstart %d)" % (newend ,  end, overlapstart ))
                log.append( "NEW C END     %d ( cend %d + overlapstart %d)" % (newcend, cend, overlapstart ))

                lines[-1][2]  = newend
                lines[-1][7]  = newcend

                log.append( "LAST LINE " + "\t".join([str(x) for x in lines[-1]]))

                start  = lines[-1][1]
                end    = lines[-1][2]
                cstart = lines[-1][6]
                cend   = lines[-1][7]
                final  = end + 1

                log.append( "START   a     %d" % start  )
                log.append( "END     a     %d" % end    )
                log.append( "cSTART  a     %d" % cstart )
                log.append( "cEND    a     %d" % cend   )


            assert end >= start     , "\n".join( log )
            assert lines[-1][2] > 0 , "\n".join( log )
            assert lines[-1][7] > 0 , "\n".join( log )


            linestr = ""
            for line in lines:
                linestr += "\t".join([str(x) for x in line[:-1]]) + "\n"
            print "PRINTING FIXED", linestr

        log.append( "FINAL %d" % final            )


        #skip last register if it is a gap
        if lines[-1][-1]: # is gap
            end = lines[-1][2]

            assert final == end + 1, "\n".join( log )

            #print "LINES BEFORE", lines

            a = lines.pop(-1)

            #print "LINES AFTER", lines
            #print "OUT", a

            linestr = ""
            for line in lines:
                linestr += "\t".join([str(x) for x in line[:-1]]) + "\n"

            #print linestr
            #sys.exit(1)
            end   = lines[-1][2]

            final = end + 1

        log.append( "FINAL %d" % final            )



        if final != (scaffold_len+1):
            print 'ERROR - SIZES DO NOT MATCH'
            print "\n".join(log)

            print "ERROR LAST"
            print " contig id     %s contig len %8d start %8d" % (contig_id, contig_len, start)
            print "  start        %8d" % start
            print "  end          %8d" % end
            print "  overlapstart %8d" % overlapstart
            print "  overlapend   %8d" % overlapend
            print "  begindiff    %8d" % begindiff
            print "  contig_len   %8d" % contig_len
            print "  contig_start %8d" % contig_start

            print "final size %d != scaffold length %d diff %d" % ( final, scaffold_len, (final - scaffold_len))
            print scaffold_key
            #print "\n".join( [str("\t".join([str(y) for y in x])) for x in contig_data] )

            for cdata in contig_data:
                lbl = ""
                for pos in range(len(cdata)):
                    lbl += ' ' + labels[pos] + ': ' + str(cdata[pos]) + ','
                print lbl

            print line
            #sys.exit( 1 )



        if final > (scaffold_len+1):
            end    = lines[-1][2]
            cend   = lines[-1][7]
            diff   = final - (scaffold_len+1)
            end   -= diff
            cend  -= diff
            if end > 0 and cend > 0:
                print "LAST SALVATION"
                lines[-1][2] =  end
                lines[-1][7] = cend

                linestr      = ""
                for line in lines:
                    linestr += "\t".join([str(x) for x in line[:-1]]) + "\n"


        ofhd.write( linestr )
        #print line


    ofhd.close()
    print "CLOSING",ofile


#This file contains a list of all the supercontigs in the final assembly.
#For each, we give the numbers and lengths of the constituent contigs, as well
#as the approximate gaps between them (if a positive number is shown in
#parentheses) or the approximate overlap between them (if a negative number
#is shown).
#
#scaffold 0 (l = 990459):
#0 (l = 6806)
# -- (10 +/- 33) --> 1 (l = 2499)
# -- (15 +/- 36) --> 2 (l = 26126)
# -- (85 +/- 30) --> 3 (l = 12801)
# -- (-35 +/- 20) --> 4 (l = 35498)
# -- (21 +/- 14) --> 5 (l = 23087)
# -- (1540 +/- 248) --> 30 (l = 13492)
# -- (1600 +/- 271) --> 31 (l = 3033)
# -- (170 +/- 38) --> 32 (l = 44153)
#
#scaffold 1 (l = 895304):
#33 (l = 2123)


def main():
    parser = argparse.ArgumentParser(description="Prepare Files for NCBI submission", epilog="python NCBI_fixer.py -i habrochaites/final.assembly.fasta -r 'scaffold_(\d+)/Sh_\1 [organism=Solanum habrochaites][cultivar=LYC4]' -x scaffold_5444 -t scaffold_5444:50..70")
    parser.add_argument('-s', '--scaf' , '--scaffold',       dest='scaffold'   , default=None       , type=str ,            metavar='S', help='input scaffold fasta file' )
    parser.add_argument('-c', '--cont' , '--contig'  ,       dest='contig'     , default=None       , type=str ,            metavar='C', help='input contig fasta file' )
    parser.add_argument('-a', '--agp',                       dest='agp'        , default=None       , type=str , nargs='?', metavar='A', help='all paths lg scaffold information to be converted to agp' )
    parser.add_argument('-b', '--agp-conf',                  dest='agpconf'    , default=None       , type=str , nargs='?', metavar='B', help='configuration to agp' )

    parser.add_argument('-x', '--exclude',                   dest='exclude'    , default=[]         , type=str , nargs='*', metavar='X', help='list of excluded IDs (before renaming): scaffold_123' )
    parser.add_argument('-r', '--sren', '--scaffold-rename', dest='srename'    , default=[]         , type=str , nargs='*', metavar='R', help='scaffold rename commands separated by /: "scaffold_(\d+)/Sh_scaffold_\1 [organism=Solanum habrochaites][cultivar=LYC4]"')
    parser.add_argument('-q', '--cren', '--contig-rename',   dest='crename'    , default=[]         , type=str , nargs='*', metavar='Q', help='contig rename commands separated by /: "contig_(\d+)/Sh_contig_\1 [organism=Solanum habrochaites][cultivar=LYC4]"')
    parser.add_argument('-t', '--trim',                      dest='trim'       , default=[]         , type=str , nargs='*', metavar='T', help='trim sequence in the format "NAME:START..END"')
    parser.add_argument('--min_seq_len'          ,           dest='min_seq_len', default=MIN_SEQ_LEN, type=int , nargs='?', metavar='M', help='minimum length of sequence [dfl: '+str(MIN_SEQ_LEN)+']' )

    parser.add_argument('-n', '--np', '--noprint',           dest='writefasta' , action='store_false'                                  , help='do not export fasta file')
    args = parser.parse_args()

    scaffold    = args.scaffold
    contig      = args.contig
    agpfile     = args.agp
    agpfileconf = args.agpconf
    agp         = None
    global writefasta
    writefasta  = args.writefasta

    print "write fasta", writefasta

    if scaffold is not None:
        if not os.path.exists( scaffold ):
            print "input scaffold file %s does not exists" % scaffold
            sys.exit( 1 )


    if contig is not None:
        if not os.path.exists( contig ):
            print "input contig file %s does not exists" % contig
            sys.exit( 1 )


    if agpfile is not None:
        agp = parseAgp( agpfile, agpfileconf, args )

    if scaffold is not None:
        parseFile( args, scaffold, agp )

    if contig   is not None:
        parseFile( args, contig  , agp , contig=True)


    if agpfileconf is not None:
        agpout = agpfile + '.agp'

        if scaffold is not None:
            agpout = scaffold + '.agp'

        printAgp(agp, agpout)



if __name__ == '__main__': main()
